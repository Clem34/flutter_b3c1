import 'package:application/view/location_list.dart';
import 'package:application/view/login_page.dart';
import 'package:application/view/profil.dart';
import 'package:application/view/share/botnavbar.dart';
import 'package:application/view/validation_location.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart';
import 'package:application/models/habitation.dart';
import 'package:application/models/type_habitat.dart';
import 'package:application/services/habitation_service.dart';
import 'package:application/share/location_style.dart';
import 'package:application/share/location_text_style.dart';
import 'package:application/view/habitation_list.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key : key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Locations',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title:'Mes locations'),
      initialRoute: '/',
      routes: {
        'Profil/': (context) => const Profil(),
        'LoginPage/': (context) => const  LoginPage(), 
        'LocationList/': (context) => const  LocationList(), 
        'ValidationLocation/': (context) => const  ValidationLocation(), 
      },
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [Locale('en'), Locale('fr')],
    );
  }
}


class MyHomePage extends StatelessWidget {
  
  final HabitationService service = HabitationService();
  final String title;
  late final List<TypeHabitat> _typehabitats;
  late final List<Habitation> _habitations;

  MyHomePage({
    Key? key,
    required this.title,
  }) : super(key: key){
    _typehabitats = service.getTypeHabitat();
    _habitations = service.getHabitationTop10();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('title'),
      ),
      bottomNavigationBar:  const BottomNavigationBarWidget(1),
      body: Center(
        child: Column(
          children: [
            const SizedBox(height: 30,),
              _buildTypeHabitat(context),
            const SizedBox(height: 30,),
              _buildDerniereLocation(context),
          ]
        ),
      ),
    );
  }


  _buildTypeHabitat(BuildContext context) {
    return SizedBox(
      height: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: List.generate(
          _typehabitats.length,
          (index) => _buildHabitat(context, _typehabitats[index]),
        ),
      ),
    );
  }

  _buildHabitat(BuildContext context, TypeHabitat typeHabitat) {
    var icon = Icons.house;
    switch (typeHabitat.id) {
      case 2:
      icon = Icons.apartment;
      break;
      default:
      icon = Icons.home;
    }
    return Expanded(    
      child: Container(
        height: 80,
        decoration: BoxDecoration(
          color: LocationStyle.backgroundColorPurple,
          borderRadius: BorderRadius.circular(8.0)
        ),
        margin: const EdgeInsets.all(8.0),
        child: InkWell(
          onTap: () {
            Navigator.push(context,
              MaterialPageRoute(
                builder: (context) => 
                  HabitationList(typeHabitat.id == 1),
              )
            );
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(icon, color: Colors.white70),
              const SizedBox(width: 5,),
              Text(typeHabitat.libelle, style: LocationTextStyle.regularWhiteTextStyle,),
            ],
          ),
        ),
      ),
    );
  }

  _buildDerniereLocation(BuildContext context) {
    return SizedBox(
      height: 240,
      child: ListView.builder(
        itemCount: _habitations.length,
        itemExtent: 220,
        itemBuilder: (context, index) => 
          _buildRow(_habitations[index], context),
        scrollDirection: Axis.horizontal,
        ),
    );
  }

  _buildRow(Habitation habitation, BuildContext context) {
    var format = NumberFormat("### €");
    
    return Container(
      width: 240,
      margin: const EdgeInsets.all(4.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Image.asset(            
              'assets/images/locations/${habitation.image}',
              fit: BoxFit.fitWidth,
            ),
          ),
          Text(habitation.libelle, style: LocationTextStyle.regularTextStyle),
          Row(
            children: [
              const Icon(Icons.location_on_outlined),
              Text(habitation.adresse, style: LocationTextStyle.regularTextStyle,),
            ],
          ),
          Text(format.format(habitation.prixmois), style: LocationTextStyle.boldTextStyle,),
        ],
      ),
    );
  }
}
