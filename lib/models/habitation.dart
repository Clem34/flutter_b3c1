import 'package:application/models/type_habitat.dart';

class Habitation {
  Habitation(
      this.id, 
      this.typeHabitat, 
      this.image, 
      this.libelle, 
      this.adresse, 
      this.nbpersonnes, 
      this.chambres, 
      this.lits, 
      this.salleBains,
      this.superficie, 
      this.prixmois,
      {
        this.options = const [], 
        this.optionpayantes = const []
      });


  int id;
  TypeHabitat typeHabitat;
  String image;
  String libelle;
  String adresse;
  int nbpersonnes;
  int chambres;  
  int lits;
  int salleBains;
  int superficie;
  double prixmois;
  List<Option> options;
  List<OptionPayante> optionpayantes;

  Habitation.fromJson(Map<String, dynamic> json)
  :id = json['id'],
  typeHabitat  = TypeHabitat.fromJson(json['typehabitat']),
  image = json['image'],
  libelle = json['libelle'],
  adresse = json['adresse'],
  nbpersonnes = json['habitantsmax'],
  chambres = json['chambres'],
  lits = json['lits'],
  salleBains = json['sdb'],
  superficie = json['superficie'],
  prixmois = json['prixmois'],
  options = (json['items'] as List).map((item) => Option.fromJson(item)).toList(),
  optionpayantes = (json['optionpayantes'] as List).map((item) => OptionPayante.fromJson(item)).toList();

}


class Option {  
  Option(this.id, this.libelle, {this.description = ""});
  
  int id;
  String libelle;
  String description;

    Option.fromJson(Map<String, dynamic> json)
  :id = json['id'],
   libelle = json['libelle'],
  description = json['description'];
}

class OptionPayante extends Option {
  OptionPayante(super.id, super.libelle, {super.description = "", this.prix = 0});

  double prix;

  OptionPayante.fromJson(Map<String, dynamic> json)
  : prix = json['prix'],
  super.fromJson(json['optionpayante']);
}



