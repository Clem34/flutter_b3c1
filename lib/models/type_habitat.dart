
class TypeHabitat {

  TypeHabitat(this.id,this.libelle);

  int id;
  String libelle;

    TypeHabitat.fromJson(Map<String, dynamic> json)
  :id = json['id'],
  libelle = json['libelle'];


static TypeHabitat getMaison(){
  return TypeHabitat(1, "Maison");
}

static TypeHabitat getAppartement(){
  return TypeHabitat(2, "Appartement");
}

}