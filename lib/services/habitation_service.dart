import 'package:application/models/habitation.dart';
import 'package:application/models/habitations_data.dart';
import 'package:application/models/type_habitat.dart';
import 'package:application/models/typehabitat_data.dart';

class HabitationService {

var _typehabitats;
late List<Habitation> _habitations;

  HabitationService() {
    _typehabitats = TypehabitatData.buildList();
    _habitations = HabitationsData.buildList();
  }

  List<TypeHabitat> getTypeHabitat() {
    return _typehabitats;
  }

  // final _typeHabitats = [TypeHabitat(1, "Maison"), TypeHabitat(2, "Appartement")];

  List<Habitation> getHabitationTop10() {
    return _habitations.where((element) => element.id%2 == 1).take(10).toList();
  }

  List<Habitation> getMaisons() {
    return _getHabitations(isHouse: true);
  }

  List<Habitation> getAppartements() {
    return _getHabitations(isHouse: true);
  }

  List<Habitation> _getHabitations({bool isHouse = true}) {
    return _habitations.where((element) => element.typeHabitat.id == (isHouse ? 1 : 2)).toList();
  }
}