import 'package:application/models/location.dart';
import 'package:application/services/location_service.dart';
import 'package:application/services/locations_data.dart';

class LocationApiData implements LocationApiClient {
  @override
  Future<List<Location>> getLocations() {
    return Future.delayed(
      const Duration(seconds: 1),
      () => LocationsData.buildList()); 
  }

  @override
  Future<Location> getLocation(int id) {
    Location location = 
      LocationsData.buildList().
      where((element) => element.id == id).first;

  return Future.delayed(
    const Duration(seconds: 1),
    () => location);
  }
}