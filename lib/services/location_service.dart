import 'package:application/models/location.dart';
import 'package:application/services/location_api_client_lmpl.dart';
import 'package:application/services/location_api_data.dart';

class LocationService {
  final LocationApiClient locationApiClient;

  LocationService() : 
    locationApiClient = LocationApiClientlmpl();

  Future<List<Location>> getLocations() async {
    List<Location> list = await locationApiClient.getLocations();
    return list;
  }

  Future<Location> getLocation(int id) {
    return locationApiClient.getLocation(id);
  }
}

abstract class LocationApiClient {
  Future<List<Location>> getLocations();
  Future<Location> getLocation(int id);
}