import 'package:application/models/habitation.dart';
import 'package:application/share/location_style.dart';
import 'package:application/share/location_text_style.dart';
import 'package:application/view/resa_location.dart';
import 'package:application/view/share/botnavbar.dart';
import 'package:application/view/share/habitation_features_widget.dart';
import 'package:application/view/share/habitation_option.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class HabitationDetails extends StatefulWidget {

  final Habitation _habitation;
  const HabitationDetails(this._habitation, {Key? key}) : super(key: key);

  @override
  State<HabitationDetails> createState() => _HabitationDetailsState();
}

class _HabitationDetailsState extends State<HabitationDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._habitation.libelle),
      ),
      bottomNavigationBar: const BottomNavigationBarWidget(2),
      body: ListView(
        padding: const EdgeInsets.all(4.0),
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: SizedBox(
              width: 500,
              height: 500,
              child: Image.asset(
                'assets/images/locations/${widget._habitation.image}',
                fit: BoxFit.fitWidth,
              ),
            ),  
          ),
          Container(margin: const EdgeInsets.all(8.0),
          child: Text(widget._habitation.adresse),
          ),
          HabitationFeaturesWidget(widget._habitation),
          _buildItems(),
          _buildOptionsPayantes(),
          _buildRentButton(),
        ],
      ),
    );
  }

  _buildItems() {
    // var width = (MediaQuery.of(context).size.width / 2) -15;
  
    return Column(
      children: [
        Row(
          children: [
            Container(
              padding: const EdgeInsets.only(right: 8),
              child: Text("Inclus", style: LocationTextStyle.subTitleboldTextStyle,),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 10),
              child: const Divider( 
                height: 36,
                thickness: 1,

              ),
            ),
          ]),
        widget._habitation.options.isEmpty
        ? Container()
        : Wrap(
        spacing: 2.0,
        children: 
          Iterable.generate(widget._habitation.options.length,
          (i) => Container(
            padding: const EdgeInsets.only(left: 15),
            margin: const EdgeInsets.all(2),
            child: Column(
              children: [
                Text(widget._habitation.options[i].libelle),
                Text(widget._habitation.options[i].description, style:LocationTextStyle.regularGreyTextStyle,)
              ],
            ),
          ),
          ).toList(),
        ),
      ],
    );
  }

  _buildOptionsPayantes() {
    // var width = (MediaQuery.of(context).size.width / 2) -15;
  if(widget._habitation.optionpayantes.isEmpty){
    return Container();
  }
  return Wrap(
    spacing: 2.0,
    children: Iterable.generate(widget._habitation.optionpayantes.length,
    (i) => Container(
      padding: const EdgeInsets.only(left: 15),
      margin: const EdgeInsets.all(2),
      child: Row(
        children: [
          Text(widget._habitation.optionpayantes[i].libelle),
          Text(widget._habitation.optionpayantes[i].prix.toString(), style:LocationTextStyle.regularGreyTextStyle,)
        ],
      ),
    ),
    ).toList(),
  );
  }

  _buildRentButton() {
    var format = NumberFormat("### €");

    return Container(
      decoration: BoxDecoration(
        color: LocationStyle.backgroundColorPurple,
        borderRadius: BorderRadius.circular(8.0),
      ),
      margin: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              format.format(widget._habitation.prixmois),
              style: LocationTextStyle.regularWhiteTextStyle,
            ),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.purple,
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              textStyle: const TextStyle(
              fontSize: 30,
              color: Colors.white,
              fontWeight: FontWeight.bold)),
            onPressed: () {
              Navigator.push(
                context, 
                MaterialPageRoute(
                  builder: ((context) => ResaLocation(habitation: widget._habitation)),
                ),
              );
            },
            child: Text("Réserver", style: LocationTextStyle.regularTextStyle.copyWith(color: Colors.white),),
          ),
        ],
      ),
    );
  }
}