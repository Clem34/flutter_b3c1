import 'package:application/models/habitation.dart';
import 'package:application/models/type_habitat.dart';
import 'package:application/view/habitation_details.dart';
import 'package:application/view/share/botnavbar.dart';
import 'package:application/view/share/habitation_features_widget.dart';
import 'package:application/view/share/habitation_option.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../services/habitation_service.dart';

class HabitationList extends StatelessWidget {

  final HabitationService service = HabitationService();
  late List<Habitation> _habitations;
  final bool isHouseList;


  HabitationList(this.isHouseList, {Key? key}) : super(key: key){
    _habitations = isHouseList ? service.getMaisons() : service.getAppartements();
  }


  // var _habitations = [
  //   Habitation(2, TypeHabitat.getAppartement() ,"appartement.png", "appartement 2 rue 2",2, 3, 50, 555, 3),
  //   Habitation(3, TypeHabitat.getAppartement(),"appartement.png", "appartement 3", "rue 3", 2, 51, 401, 2),
  //   Habitation(4, TypeHabitat.getAppartement(),"appartement.png", "appartement 4", "rue 4", 2, 51, 402, 2),
  // ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Liste des ${isHouseList ? 'maisons' : 'appartements'}"),
      ),
      bottomNavigationBar: const BottomNavigationBarWidget(2),
      body: Center(
        child: ListView.builder(
          itemCount: _habitations.length,
          itemBuilder: (context, index) => 
            _buildRow(_habitations[index], context),
          itemExtent: 285,  
            ),
      ),
    );
  }

  _buildRow(Habitation habitation, BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(4.0),
      child: GestureDetector(
        onTap: (){
          Navigator.push(
            context, 
            MaterialPageRoute(
            builder: ((context) => HabitationDetails(habitation)),
            ),
          );
        },
        child: Column(
          children: [
            Container(
              height: 150,
              width: MediaQuery.of(context).size.width,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: Image.asset('assets/images/locations/${habitation.image}',
                  fit: BoxFit.fitWidth,
                  ),
                ),
            ),
            _buildDetails(habitation),
          ]),
      ),
    );
  }

  _buildDetails(Habitation habitation){
    var format = NumberFormat("### €");
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 3,
                child: ListTile(
                  title: Text(habitation.libelle),
                  subtitle: Text(habitation.adresse),
                ),
              ),
              Expanded(
                child: Text(format.format(habitation.prixmois),
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto',
                    fontSize: 22,
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              HabitationOption("${habitation.chambres} personnes", Icons.group),
              HabitationOption("${habitation.superficie} m²", Icons.fit_screen),
            ],
          ),
          HabitationFeaturesWidget(habitation),
        ],
      ),
    );
  }
}