import 'package:application/models/location.dart';
import 'package:application/services/location_service.dart';
import 'package:application/view/share/botnavbar.dart';
import 'package:flutter/material.dart';


class LocationList extends StatefulWidget {
  static String routeName = '/locations';
  const LocationList({Key? key}) : super(key: key);

  @override
  State<LocationList> createState() => _LocationListState();
}

class _LocationListState extends State<LocationList> {
  final LocationService service = LocationService();
  late Future<List<Location>> _locations;

  @override
  void initState() {
    super.initState();
    _locations = service.getLocations();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Mes locations"),
      ),     
      body: Center(
        child: Container(
          child: Text('FUTUREBUILDER'),
        ),
      ),
      bottomNavigationBar: const BottomNavigationBarWidget(2),
    );
  }

}
