import 'package:application/models/habitation.dart';
import 'package:application/share/location_text_style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'share/botnavbar.dart';


class ResaLocation extends StatefulWidget {
  
  const ResaLocation({super.key, required this.habitation});
  final Habitation habitation;

  @override
  State<ResaLocation> createState() => _ResaLocationState();
}

class _ResaLocationState extends State<ResaLocation> {
  DateTime dateDebut = DateTime.now();
  DateTime dateFin = DateTime.now();
  String nbPersonnes = '1';
  List<String> list = <String>['1', '2', '3', '4', '5', '6', '7', '8'];
  double sommesOptions = 0;
  List<OptionPayanteCheck> optionPayanteChecks = [];

  var format = NumberFormat("### €");
  late Habitation _habitation;
  Map<int, bool> mapOptions = <int, bool>{};

  @override
  void initState(){
    super.initState();
    _habitation = widget.habitation;
    for(OptionPayante optionPayante in  _habitation.optionpayantes){
      mapOptions[optionPayante.id] = false;
    }
    
  }


  @override
Widget build(BuildContext context) {
  //List<OptionPayante> loadOptionPayantes = [];
  return Scaffold(
    appBar: AppBar(
      title: const Text('Réservation')
    ),
    bottomNavigationBar: BottomNavigationBarWidget(2),
    body: ListView(
      padding: const EdgeInsets.all(4.0),
      children: [
        _buildResume(),
        _buildDates(context),
        _buildNbPersonnes(context),
        _buildOptionsPayantes(context),
        TotalWidget(context),
        _buildRentButton(),
      ],
    ),
  );
}

Widget _buildResume() {
  return  ListTile(
    leading: const Icon(Icons.home),
    title: Text(_habitation.libelle, style: LocationTextStyle.regularGreyTextStyle ),
    subtitle: Text(_habitation.adresse, style: LocationTextStyle.regularTextStyle),
  );
}
  
Widget _buildDates (BuildContext context) {
  return  GestureDetector(
      onTap: dateTimeRangePicker,
      child: Row(
        children: [
          const Icon(Icons.calendar_month),
          Text(dateDebut.toString()),
          const CircleAvatar(
            backgroundColor: Colors.blueAccent,
            child: Icon(Icons.arrow_right),
          ),
          const Icon(Icons.calendar_month),
          Text(dateFin.toString()),
        ],
      ),
    );
    //backgroundColor: Colors.blueAccent,
}

void dateTimeRangePicker() async {
    DateTimeRange? datePicked = await showDateRangePicker(
      context: context,
      firstDate: DateTime(DateTime.now().year),
      lastDate: DateTime(DateTime.now().year + 2),
      initialDateRange: DateTimeRange(start: dateDebut, end: dateFin),
      cancelText: 'Annuler',
      confirmText: 'Valider',
      locale: const Locale("fr", "FR"),
    );
    if (datePicked != null) {
      setState(() {
        dateDebut = datePicked.start;
        dateFin = datePicked.end;
      });
    }
  }

  Widget _buildNbPersonnes(BuildContext context) {
    String dropdownValue = list.first;
    return Container(
      child: Row(
        children: [
          const Text("Nombre de personnes"),
          DropdownButton<String>(
            value: dropdownValue,
            hint: const Text("nombres de personnes") ,
            items: list.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
            onChanged: (String? value) {
        // This is called when the user selects an item.
        setState(() {
          dropdownValue = value!;
        });
      },)
        ],
      )
    );
  }

Widget _buildOptionsPayantes(BuildContext context) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: _getWidgetOptionPayantes(context)
  );
}

List<Widget> _getWidgetOptionPayantes (BuildContext context){
  List <Widget> mesWidget =  <Widget>[];
  for (int i = 0; i < _habitation.optionpayantes.length; i++ ) {
    mesWidget.add(_getOptionPayante(context, _habitation.optionpayantes[i]));
  }
  return mesWidget;
}

  Widget _getOptionPayante(BuildContext context, OptionPayante optionpayantes ) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        const SizedBox(
          height: 30,
          width: 30, 
          child: Center(
            child: Icon(
              color: Colors.grey, Icons.add_shopping_cart
            )
          ),
        ),
        
        Expanded(
          child: SizedBox(
            height: 60,
            child: CheckboxListTile(
              value: mapOptions[optionpayantes.id],
              onChanged:(bool? value) {
                setState(() {
                  
                  if(value != null && value){
                    mapOptions[optionpayantes.id] = true;
                    // Recalcule le total
                    sommesOptions = sommesOptions + optionpayantes.prix;
                  }
                  else{
                    mapOptions[optionpayantes.id] = false;
                    sommesOptions = sommesOptions - optionpayantes.prix;
                  }
                });
              },
              title: SizedBox(height : 30, child: Text(optionpayantes.libelle)),
              subtitle:  Padding(
                padding: const EdgeInsets.all(2.0),
                child: Text(optionpayantes.description),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget TotalWidget(context) {
    return Container(
      width: 500,
      decoration: BoxDecoration(
        color: Colors.purple,
        border: Border.all(width: 1),
        borderRadius: const BorderRadius.all(Radius.circular(20)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween ,
        children: [
          Text("TOTAL"),
          Text("${sommesOptions + _habitation.prixmois} €")
        ],
      ),
    );
  }

  Widget _buildRentButton() {
    return GestureDetector(
      child: Container(
        child: Container(),
        decoration: BoxDecoration(
          border: Border.all(width: 1),
        ),
      ),
    );
  }

}



class OptionPayanteCheck extends OptionPayante {
  bool checked;

  OptionPayanteCheck(super.id, super.libelle, this.checked, {super.description = "", super.prix});




}