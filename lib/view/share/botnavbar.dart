import 'package:application/view/location_list.dart';
import 'package:application/view/profil.dart';
import 'package:application/view/share/badge_widget.dart';
import 'package:flutter/material.dart';


class BottomNavigationBarWidget extends StatelessWidget {
  final int indexSelected;
  const BottomNavigationBarWidget(this.indexSelected,{super.key});

  @override
  Widget build(BuildContext context) {
    bool isUserNotConnected = true;

    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: indexSelected,
      items: <BottomNavigationBarItem>[
        const BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Accueil',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.search),
          label: 'Recherche',
        ),
        BottomNavigationBarItem(
          icon: isUserNotConnected
            ? const Icon(Icons.shopping_cart_outlined) : BadgeWidget(value: 0, top: 0, right: 0, color: null, child: const Icon(Icons.shopping_cart)),
          label: 'locations',
        ), 
        const BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'Profil',
        ),     
      ],
      onTap: (index) {
        String page = '/';
        switch (index) {
          case 2:
            page = 'LocationList/';
            break;
          case 3:
            page = 'Profil/';
            break;
        }
        Navigator.pushNamedAndRemoveUntil(
          context, 
          page, 
          (route) => false,
        );
      },
    );
  }
}