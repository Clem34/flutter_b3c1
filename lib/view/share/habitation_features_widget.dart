import 'package:application/view/share/habitation_option.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../models/habitation.dart';

class HabitationFeaturesWidget extends StatelessWidget {
  final Habitation _habitation;
  const HabitationFeaturesWidget(this._habitation,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        HabitationOption("${_habitation.chambres} personnes", Icons.group),
        HabitationOption("${_habitation.chambres} chambres", Icons.bed),
        HabitationOption("${_habitation.superficie} personnes", Icons.fit_screen),
      ],
    );
  }


}
